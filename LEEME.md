# DEMO_UTP_JAVA.

PROYECTO REALIZADO:
 1. JAVA 1.8.0_271
 2. Apache Tomcat/9.0.41
 3. BASE DE DATOS MYSQL (nombre BBDD: utp, se adjunta diagrama y codigo sql de importacion)  
 4. Framework SPRING MVC 4.1 (No se utilizan las vistas)
 5. Gestionado las dependencias con Maven


 6. Métodos implementados (Endpoint):
    1.  Acceso 
        1.1 Login (Creacion del JWT) - URL (ip:puerto/demoUtp/acceso/login)
    2. Nota
        2.1 Listar Nota - Si el usuario tiene rol= 1 (admin) consulta nota de cualquier alumno y
                rol = 2 (estudiante) consulta su propia nota,  otro rol es denegado el acceso.
                - URL (ip:puerto/demoUtp/nota/listarNotas)

        2.2 Agregar Nota - Solo puede ingresa Nota el usuario con rol = 1, otro rol es denegado el acceso
                - URL (ip:puerto/demoUtp/nota/registrarNotas)

 7. Parámetros de entradas (POST) y salidas en formato JSON
 
 8. Estructura de respuesta en formato JSON:
        [
            codigoError: (1 o 0),
            mensaje: (descripcion si el proceso se ejecuto correo o incorrecto),
            info: (OPCIONAL, Solo presente para el endpoint ip:puerto/demoUtp/acceso/login, devuelve el JWT)
            listaDataObject: (OPCIONAL,  Solo presente para el endpoint ip:puerto/demoUtp/nota/listarNotas, devuelve el listado de notas del alumno)
        ]

 9. Antes de realizar la lógica el endpoint invocado, realizará la validación del JWT (Si es correcta la validación continúa la lógica,
    sino retornará  un mensaje).

 