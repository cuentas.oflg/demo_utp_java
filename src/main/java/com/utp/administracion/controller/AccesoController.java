package com.utp.administracion.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import com.utp.administracion.controller.base.ResponseGenerico;
import com.utp.administracion.domain.Acceso;
import com.utp.administracion.serviceImpl.AccesoService;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author OFLG
 */
@RestController
@EnableWebMvc
@RequestMapping("/acceso")
public class AccesoController {

    final static Logger logger = Logger.getLogger(AccesoController.class);

    @Autowired
    AccesoService accesoService;

    @PostMapping(value = "/login", produces = "application/json", consumes = "application/json")
    public ResponseGenerico<Acceso> login(@RequestBody Acceso object) {
        ResponseGenerico<Acceso> responseGenerico;
        responseGenerico = accesoService.login(object);
        responseGenerico.setHttpStatus(HttpStatus.OK);
        return responseGenerico;
    }

}
