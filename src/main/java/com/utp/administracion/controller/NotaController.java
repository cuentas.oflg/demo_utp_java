package com.utp.administracion.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import com.utp.administracion.controller.base.ResponseGenerico;
import com.utp.administracion.domain.Alumno;
import com.utp.administracion.domain.Nota;
import com.utp.administracion.serviceImpl.NotaService;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author OFLG
 */
@RestController
@EnableWebMvc
@RequestMapping("/nota")
public class NotaController {

    final static Logger logger = Logger.getLogger(NotaController.class);

    @Autowired
    NotaService notaService;

    @PostMapping(value = "/listarNotas", produces = "application/json", consumes = "application/json")
    public ResponseGenerico<Nota> listarNotas(@RequestBody Alumno object) {
        ResponseGenerico<Nota> responseGenerico;
        responseGenerico = notaService.listarNotas(object);
        responseGenerico.setHttpStatus(HttpStatus.OK);
        return responseGenerico;
    }

    @PostMapping(value = "/registrarNotas", produces = "application/json", consumes = "application/json")
    public ResponseGenerico<Nota> registrarNotas(@RequestBody Nota object) {
        ResponseGenerico<Nota> responseGenerico;
        responseGenerico = notaService.registrarNotas(object);
        responseGenerico.setHttpStatus(HttpStatus.OK);
        return responseGenerico;
    }
    

}
