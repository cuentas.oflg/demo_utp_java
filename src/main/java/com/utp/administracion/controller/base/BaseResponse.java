package com.utp.administracion.controller.base;

import org.springframework.http.HttpStatus;

/**
 *
 * @author OFLG
 */
public class BaseResponse {

    private String mensaje;
    private String info;
    private HttpStatus httpStatus;

    public BaseResponse() {;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
