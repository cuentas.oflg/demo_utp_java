package com.utp.administracion.controller.base;

import java.util.List;
import java.util.Map;

/**
 *
 * @author OFLG
 */
public class ResponseGenerico<T> extends BaseResponse {

    private Integer codigoError;
    private T dataObject;
    private List<T> listaDataObject;
    private Map<String, Object> estados;
    private Integer existe;

    public Integer getExiste() {
        return existe;
    }

    public void setExiste(Integer existe) {
        this.existe = existe;
    }

    public ResponseGenerico() {;
    }

    public T getDataObject() {
        return dataObject;
    }

    public Map<String, Object> getEstados() {
        return estados;
    }

    public void setEstados(Map<String, Object> estados) {
        this.estados = estados;
    }

    public void setDataObject(T dataObject) {
        this.dataObject = dataObject;
    }

    public List<T> getListaDataObject() {
        return listaDataObject;
    }

    public void setListaDataObject(List<T> listaDataObject) {
        this.listaDataObject = listaDataObject;
    }

    public Integer getCodigoError() {
        return codigoError;
    }

    public void setCodigoError(Integer codigoError) {
        this.codigoError = codigoError;
    }

}
