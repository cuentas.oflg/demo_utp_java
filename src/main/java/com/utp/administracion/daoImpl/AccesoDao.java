package com.utp.administracion.daoImpl;

import com.utp.administracion.controller.base.ResponseGenerico;
import com.utp.administracion.domain.Acceso;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import javax.xml.bind.DatatypeConverter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author OFLG
 */
@Repository("AccesoDao")

public class AccesoDao extends JdbcDaoSupport {

    final static Logger log = Logger.getLogger(AccesoDao.class);
    JdbcTemplate jdbcTemplate;

    @Qualifier("edataSource")
    @Autowired
    private DataSource edataSource;

    @PostConstruct
    private void initialize() {
        setDataSource(edataSource);
    }

    public ResponseGenerico<Acceso> login(Acceso object) {
        ResponseGenerico<Acceso> responseGenerico = new ResponseGenerico<>();
        Map<String, Object> mapaOut;
        try {
            SimpleJdbcuCall jdbcCall = new SimpleJdbcCall(edataSource)
                    .execute("SELECT acceso_id, usuario, clave, rol, alumno.nombre FROM acceso  WHERE usuario = ? and clave = ?");
            MapSqlParameterSource parametrosIn = new MapSqlParameterSource();
            parametrosIn.addValue("usuario", object.getUsuario());
            parametrosIn.addValue("clave", object.getClave());
            mapaOut = jdbcCall.execute(parametrosIn);
            List<Map<String, Object>> listaCursor = (List) mapaOut.get("RESULTADO");

            Acceso element;
            List<Acceso> lista = new ArrayList<>();

            for (Map<String, Object> map : listaCursor) {
                element = new Acceso();
                element.setUsuario((String) map.get("usuario"));
                element.setClave((String) map.get("clave"));
                element.setRol(((BigDecimal) map.get("rol")).intValue());
                lista.add(element);
            }

            responseGenerico.setCodigoError(0);
            responseGenerico.setMensaje("OK");
            responseGenerico.setListaDataObject(lista);

        } catch (Exception ex) {
            responseGenerico.setCodigoError(1);
            responseGenerico.setMensaje("ERROR");
            ex.printStackTrace();
        }
        return responseGenerico;
    }

    public static Claims ValidarJWT(String jwt, String llave) {
        Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(llave))
                .parseClaimsJws(jwt).getBody();
        return claims;
    }

}
