package com.utp.administracion.daoImpl;

import java.util.List;

/**
 *
 * @author OFLG
 */
public class GenericoDao<T> {

    private String mensaje;
    private Integer codigoError;
    private T dataObject;
    private List<T> listaDataObject;
    private List<T> listaDataObjectDos;
    private List<T> listaDataObjectTres;
    private Integer totalFilas;

    public GenericoDao() {;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Integer getCodigoError() {
        return codigoError;
    }

    public void setCodigoError(Integer codigoError) {
        this.codigoError = codigoError;
    }

    public T getDataObject() {
        return dataObject;
    }

    public void setDataObject(T dataObject) {
        this.dataObject = dataObject;
    }

    public List<T> getListaDataObject() {
        return listaDataObject;
    }

    public void setListaDataObject(List<T> listaDataObject) {
        this.listaDataObject = listaDataObject;
    }

    public List<T> getListaDataObjectDos() {
        return listaDataObjectDos;
    }

    public void setListaDataObjectDos(List<T> listaDataObjectDos) {
        this.listaDataObjectDos = listaDataObjectDos;
    }

    public List<T> getListaDataObjectTres() {
        return listaDataObjectTres;
    }

    public void setListaDataObjectTres(List<T> listaDataObjectTres) {
        this.listaDataObjectTres = listaDataObjectTres;
    }

    public Integer getTotalFilas() {
        return totalFilas;
    }

    public void setTotalFilas(Integer totalFilas) {
        this.totalFilas = totalFilas;
    }

}
