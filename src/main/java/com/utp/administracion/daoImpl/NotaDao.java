package com.utp.administracion.daoImpl;

import com.utp.administracion.controller.base.ResponseGenerico;
import com.utp.administracion.domain.Alumno;
import com.utp.administracion.domain.Curso;
import com.utp.administracion.domain.Nota;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author OFLG
 */
@Repository("NotaDao")

public class NotaDao extends JdbcDaoSupport {

    final static Logger log = Logger.getLogger(NotaDao.class);
    JdbcTemplate jdbcTemplate;

    @Qualifier("edataSource")
    @Autowired
    private DataSource edataSource;

    @PostConstruct
    private void initialize() {
        setDataSource(edataSource);
    }

    public ResponseGenerico<Nota> listarNotas(Alumno object) {
        ResponseGenerico<Nota> responseGenerico = new ResponseGenerico<>();
        Map<String, Object> mapaOut;
        try {
            SimpleJdbcuCall jdbcCall = new SimpleJdbcCall(edataSource)
                    .execute("SELECT nota.nota_id,\n"
                            + "    nota.alumno_id,\n"
                            + "    nota.curso_id,\n"
                            + "    nota.nota,\n"
                            + "    nota.estado,\n"
                            + "    alum.alumno_nombre,\n"
                            + "    alum.alumno_dni,\n"
                            + "    curs.curso_nombre,"
                            + "FROM nota "
                            + "INNER JOIN alumno alum ON alum.alumno_id = nota.alumno_id "
                            + "INNER JOIN curso curs ON curs.curso_id = nota.curso_id "
                            + " WHERE alumno_dni = ?");
            MapSqlParameterSource parametrosIn = new MapSqlParameterSource();
            parametrosIn.addValue("alumno_dni", object.getAlumno_dni());
            mapaOut = jdbcCall.execute(parametrosIn);
            List<Map<String, Object>> listaCursor = (List) mapaOut.get("RESULTADO");

            Nota element;
            List<Nota> lista = new ArrayList<>();

            for (Map<String, Object> map : listaCursor) {
                element = new Nota();
                element.setNota_id(((BigDecimal) map.get("nota_id")).intValue());
                element.setAlumno_id(((BigDecimal) map.get("alumno_id")).intValue());
                element.setCurso_id(((BigDecimal) map.get("curso_id")).intValue());
                element.setNota(((BigDecimal) map.get("nota")).intValue());
                element.setEstado((String) map.get("estado"));
                element.getAlumno().setAlumno_nombre((String) map.get("alumno_nombre"));
                element.getAlumno().setAlumno_dni((String) map.get("alumno_dni"));
                element.getCurso().setCurso_nombre((String) map.get("curso_nombre"));
                lista.add(element);
            }

            responseGenerico.setCodigoError(0);
            responseGenerico.setMensaje("OK");
            responseGenerico.setListaDataObject(lista);

        } catch (Exception ex) {
            responseGenerico.setCodigoError(1);
            responseGenerico.setMensaje("ERROR");
            ex.printStackTrace();
        }
        return responseGenerico;
    }

    public ResponseGenerico<Alumno> validarAlumno(String alumno_dni) {
        ResponseGenerico<Alumno> responseGenerico = new ResponseGenerico<>();
        Map<String, Object> mapaOut;

        try {
            SimpleJdbcuCall jdbcCall = new SimpleJdbcCall(edataSource)
                    .execute("SELECT alumno_id,\n"
                            + "alumno_nombre,\n"
                            + "alumno_apellido,\n"
                            + "alumno_dni"
                            + "FROM alumno "
                            + " WHERE alumno_dni = ?");
            MapSqlParameterSource parametrosIn = new MapSqlParameterSource();
            parametrosIn.addValue("alumno_dni", alumno_dni);
            mapaOut = jdbcCall.execute(parametrosIn);
            List<Map<String, Object>> listaCursor = (List) mapaOut.get("RESULTADO");

            Alumno element;
            List<Alumno> lista = new ArrayList<>();

            for (Map<String, Object> map : listaCursor) {
                element = new Alumno();
                element.setAlumno_nombre((String) map.get("alumno_nombre"));
                element.setAlumno_apellido((String) map.get("alumno_apellido"));
                element.setAlumno_dni((String) map.get("alumno_dni"));
                element.setAlumno_id(((BigDecimal) map.get("alumno_id")).intValue());
                lista.add(element);
            }

            responseGenerico.setCodigoError(0);
            responseGenerico.setMensaje("OK");
            responseGenerico.setListaDataObject(lista);

        } catch (Exception ex) {
            responseGenerico.setCodigoError(1);
            responseGenerico.setMensaje("ERROR");
            ex.printStackTrace();
        }
        return responseGenerico;
    }

    public ResponseGenerico<Curso> validarCurso(int curso_id) {
        ResponseGenerico<Curso> responseGenerico = new ResponseGenerico<>();
        Map<String, Object> mapaOut;

        try {
            SimpleJdbcuCall jdbcCall = new SimpleJdbcCall(edataSource)
                    .execute("SELECT curso_id,\n"
                            + "curso_nombre"
                            + "FROM curso "
                            + " WHERE curso_id = ?");
            MapSqlParameterSource parametrosIn = new MapSqlParameterSource();
            parametrosIn.addValue("curso_id", curso_id);
            mapaOut = jdbcCall.execute(parametrosIn);
            List<Map<String, Object>> listaCursor = (List) mapaOut.get("RESULTADO");

            Curso element;
            List<Curso> lista = new ArrayList<>();

            for (Map<String, Object> map : listaCursor) {
                element = new Curso();
                element.setCurso_id(((BigDecimal) map.get("curso_id")).intValue());
                element.setCurso_nombre((String) map.get("curso_nombre"));
                lista.add(element);
            }

            responseGenerico.setCodigoError(0);
            responseGenerico.setMensaje("OK");
            responseGenerico.setListaDataObject(lista);

        } catch (Exception ex) {
            responseGenerico.setCodigoError(1);
            responseGenerico.setMensaje("ERROR");
            ex.printStackTrace();
        }
        return responseGenerico;
    }

    public ResponseGenerico<Nota> registrarNotas(Nota object) {
        ResponseGenerico<Nota> response = new ResponseGenerico<>();
        Map<String, Object> mapaOut;
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(edataSource)
                    .execute("INSER INTO nota (alumno_id, curso_id, nota, estado) VALUES (?, ?, ?, ?)");
            MapSqlParameterSource parametrosIn = new MapSqlParameterSource();
            parametrosIn.addValue("PN_CODI_IMPU", object.getAlumno_id());
            parametrosIn.addValue("PV_DESS_IMPU", object.getCurso_id());
            parametrosIn.addValue("PN_PCTT_IMPU", object.getNota());
            parametrosIn.addValue("PN_FECH_INIC_VIGE", (object.getNota() > 11 ? "APROBADO" : "DESAPROBADO"));
            mapaOut = jdbcCall.execute(parametrosIn);
            response.setCodigoError(0);
            response.setMensaje("OK");
        } catch (Exception ex) {
            response.setCodigoError(1);
            response.setMensaje("ERROR");
            ex.printStackTrace();
        }
        return response;
    }
}
