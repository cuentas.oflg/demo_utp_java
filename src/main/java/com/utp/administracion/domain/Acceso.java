package com.utp.administracion.domain;

import java.io.Serializable;

/**
 *
 * @author OFLG
 */
public class Acceso implements Serializable {

    private String usuario;
    private String clave;
    private Integer rol;
    private String jwt;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public Integer getRol() {
        return rol;
    }

    public void setRol(Integer rol) {
        this.rol = rol;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

}
