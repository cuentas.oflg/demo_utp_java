package com.utp.administracion.domain;

import java.io.Serializable;

/**
 *
 * @author OFLG
 */
public class Alumno implements Serializable {

    private Integer alumno_id;
    private String alumno_nombre;
    private String alumno_apellido;
    private String alumno_dni;
    private Acceso acceso;

    public Integer getAlumno_id() {
        return alumno_id;
    }

    public void setAlumno_id(Integer alumno_id) {
        this.alumno_id = alumno_id;
    }

    public String getAlumno_nombre() {
        return alumno_nombre;
    }

    public void setAlumno_nombre(String alumno_nombre) {
        this.alumno_nombre = alumno_nombre;
    }

    public String getAlumno_apellido() {
        return alumno_apellido;
    }

    public void setAlumno_apellido(String alumno_apellido) {
        this.alumno_apellido = alumno_apellido;
    }

    public String getAlumno_dni() {
        return alumno_dni;
    }

    public void setAlumno_dni(String alumno_dni) {
        this.alumno_dni = alumno_dni;
    }

    public Acceso getAcceso() {
        return acceso;
    }

    public void setAcceso(Acceso acceso) {
        this.acceso = acceso;
    }

}
