package com.utp.administracion.domain;

import java.io.Serializable;

/**
 *
 * @author OFLG
 */
public class Curso implements Serializable {

    private Integer curso_id;
    private String curso_nombre;
    private Acceso acceso;

    public Integer getCurso_id() {
        return curso_id;
    }

    public void setCurso_id(Integer curso_id) {
        this.curso_id = curso_id;
    }

    public String getCurso_nombre() {
        return curso_nombre;
    }

    public void setCurso_nombre(String curso_nombre) {
        this.curso_nombre = curso_nombre;
    }

    public Acceso getAcceso() {
        return acceso;
    }

    public void setAcceso(Acceso acceso) {
        this.acceso = acceso;
    }
    

}
