package com.utp.administracion.domain;

import java.io.Serializable;

/**
 *
 * @author OFLG
 */
public class Nota implements Serializable {

    private Integer nota_id;
    private Integer alumno_id;
    private String alumno_dni;
    private Integer curso_id;
    private Integer nota;
    private String estado;
    private Acceso acceso;
    private Alumno alumno;
    private Curso curso;

    public Integer getNota_id() {
        return nota_id;
    }

    public void setNota_id(Integer nota_id) {
        this.nota_id = nota_id;
    }

    public Integer getAlumno_id() {
        return alumno_id;
    }

    public void setAlumno_id(Integer alumno_id) {
        this.alumno_id = alumno_id;
    }

    public Integer getCurso_id() {
        return curso_id;
    }

    public void setCurso_id(Integer curso_id) {
        this.curso_id = curso_id;
    }

    public Integer getNota() {
        return nota;
    }

    public void setNota(Integer nota) {
        this.nota = nota;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Acceso getAcceso() {
        return acceso;
    }

    public void setAcceso(Acceso acceso) {
        this.acceso = acceso;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public String getAlumno_dni() {
        return alumno_dni;
    }

    public void setAlumno_dni(String alumno_dni) {
        this.alumno_dni = alumno_dni;
    }

}
