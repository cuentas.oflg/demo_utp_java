package com.utp.administracion.serviceImpl;

import com.utp.administracion.controller.base.ResponseGenerico;
import com.utp.administracion.daoImpl.AccesoDao;
import com.utp.administracion.domain.Acceso;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.xml.bind.DatatypeConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author OFLG
 */
@Service("accesoService")
public class AccesoService {

    @Autowired
    AccesoDao accesoDao;

    public ResponseGenerico<Acceso> login(Acceso object) {
        ResponseGenerico<Acceso> response = accesoDao.login(object);
        if (response.getCodigoError() == 0) {
            if (response.getListaDataObject().isEmpty()) {
                ResponseGenerico<Acceso> resp = new ResponseGenerico<>();
                resp.setCodigoError(0);
                resp.setMensaje("LOGIN INCORRECTO");
                resp.setInfo("");
                return resp;
            } else {
                String llave = response.getListaDataObject().get(0).getUsuario();
                long tiempo = System.currentTimeMillis();
                String jwt = Jwts.builder()
                        .setId(llave)
                        .signWith(SignatureAlgorithm.HS256, llave)
                        .setSubject("" + response.getListaDataObject().get(0).getRol())
                        .setIssuedAt(new Date(tiempo))
                        .setExpiration(new Date(tiempo + 60 * 15000))
                        .claim("rol", response.getListaDataObject().get(0).getRol()).compact();

                ResponseGenerico<Acceso> resp = new ResponseGenerico<>();
                resp.setCodigoError(0);
                resp.setMensaje("OK");
                //resp.setListaDataObject(new ArrayList<>());
                resp.setInfo(jwt);
                return resp;
            }
        } else {
            return response;
        }
    }

}
