package com.utp.administracion.serviceImpl;

import com.utp.administracion.controller.base.ResponseGenerico;
import com.utp.administracion.daoImpl.AccesoDao;
import com.utp.administracion.daoImpl.NotaDao;
import com.utp.administracion.domain.Nota;
import com.utp.administracion.domain.Alumno;
import com.utp.administracion.domain.Curso;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author OFLG
 */
@Service("notaService")
public class NotaService {

    @Autowired
    NotaDao notaDao;
    AccesoDao accesoDao;

    public ResponseGenerico<Nota> listarNotas(Alumno object) {
        ResponseGenerico<Nota> resp = new ResponseGenerico<>();
        Claims JWT = accesoDao.ValidarJWT(object.getAcceso().getJwt(), object.getAcceso().getUsuario());

        if (JWT.getSubject().equalsIgnoreCase("" + 1)) {
            return notaDao.listarNotas(object);
        } else if (JWT.getSubject().equalsIgnoreCase("" + 2)) {
            if (!JWT.getId().equalsIgnoreCase(object.getAlumno_dni())) {
                resp.setCodigoError(1);
                resp.setMensaje("ACCESO DENEGADO");
                return resp;
            }
            return notaDao.listarNotas(object);
        } else {
            resp.setCodigoError(1);
            resp.setMensaje("ACCESO DENEGADO");
            return resp;
        }
    }

    public ResponseGenerico<Nota> registrarNotas(Nota object) {
        ResponseGenerico<Nota> resp = new ResponseGenerico<>();
        Claims JWT = accesoDao.ValidarJWT(object.getAcceso().getJwt(), object.getAcceso().getUsuario());

        if (JWT.getSubject().equalsIgnoreCase("" + 1)) {

            //VALIDAR LA NOTA
            if (object.getNota() < 0 && object.getNota() > 20) {
                resp.setCodigoError(1);
                resp.setMensaje("NOTA INCORRECTO");
                resp.setListaDataObject(new ArrayList<>());
                return resp;
            }

            // SI EXISTE EL ALUMNO INGRESADO
            ResponseGenerico<Alumno> respAlum = notaDao.validarAlumno(object.getAlumno_dni());
            if (respAlum.getCodigoError() == 1) {
                resp.setCodigoError(1);
                resp.setMensaje(respAlum.getMensaje());
                resp.setListaDataObject(new ArrayList<>());
                return resp;
            }
            if (respAlum.getListaDataObject().isEmpty()) {
                resp.setCodigoError(1);
                resp.setMensaje("NO EXISTE EL ALUMNO");
                resp.setListaDataObject(new ArrayList<>());
                return resp;
            }

            // SI EXISTE EL CURSO INGRESADO
            ResponseGenerico<Curso> respCurs = notaDao.validarCurso(object.getCurso_id());
            if (respCurs.getCodigoError() == 1) {
                resp.setCodigoError(1);
                resp.setMensaje(respCurs.getMensaje());
                resp.setListaDataObject(new ArrayList<>());
                return resp;
            }
            if (respCurs.getListaDataObject().isEmpty()) {
                resp.setCodigoError(1);
                resp.setMensaje("NO EXISTE EL CURSO");
                resp.setListaDataObject(new ArrayList<>());
                return resp;
            }

            return notaDao.registrarNotas(object);
        } else {
            resp.setCodigoError(0);
            resp.setMensaje("ACCESO DENEGADO");
            return resp;
        }
    }

}
